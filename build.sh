#!/usr/bin/env bash
mkdir -p dist
GOOS=linux GOARCH=amd64 go build -v -o dist/mip
GOOS=darwin GOARCH=amd64 go build -v -o dist/mip_darwin
GOOS=windows GOARCH=amd64 go build -v -o dist/mip.exe
ls -lah dist
