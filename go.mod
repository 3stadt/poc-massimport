module gitlab.com/3stadt/poc-massimport

go 1.12

require (
	github.com/Pallinder/go-randomdata v1.2.0
	github.com/google/uuid v1.1.1
	golang.org/x/text v0.3.2 // indirect
)
