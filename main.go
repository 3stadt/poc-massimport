package main

import (
	"flag"
	"fmt"
	"gitlab.com/3stadt/poc-massimport/producer"
	"gitlab.com/3stadt/poc-massimport/swapi"
	"log"
	"math"
	"os"
	"sync"
	"time"
)

func main() {
	a := flag.Int("amount", 0, "The total amount of products to produce.")
	t := flag.Int("threads", 0, "How many threads to use. Suggestion: Use the number of available CPU cores.")
	b := flag.Int("batchSize", 100, "Amount of products to write to the shopware API at once")
	flag.Parse()
	amount := *a
	threads := *t
	batchSize := *b
	if amount < 1 || threads < 1 {
		fmt.Println("Usage: pmi --amount=<int> --threads=<int> [--batchSize=<int>]")
		os.Exit(1)
	}
	if amount < threads {
		threads = amount
	}
	if batchSize < 1 {
		batchSize = 1
	}
	prodsWritten := 0
	extra := int(math.Mod(float64(amount), float64(threads)))
	amountPerThread := (amount - extra) / threads

	storage := swapi.Open(threads, batchSize)
	var wg sync.WaitGroup
	start := time.Now()
	for i := 0; i < threads; i++ {
		threadAmount := amountPerThread
		if extra > 0 && i == 0 {
			threadAmount = amountPerThread + extra
		}
		prodsWritten += threadAmount
		wg.Add(1)
		go producer.Produce(threadAmount, &wg, storage.ProdChannels[i])
		fmt.Printf("Thread %d running...\n", i+1)
	}
	wg.Wait()
	storage.Close()
	elapsed := time.Since(start)
	log.Printf("Produce took %s, %d products send to shopware API.", elapsed, prodsWritten)

}
