package producer

import (
	"fmt"
	"github.com/Pallinder/go-randomdata"
	"github.com/google/uuid"
	"log"
	"os"
	"sync"
)

type Storage interface {
	Save(*Product) error
}

func Produce(amount int, wg *sync.WaitGroup, s ...Storage) {
	defer wg.Done()
	log.Printf("Starting to generate %d products.", amount)

	for i := 0; i < amount; i++ {
		//if math.Mod(float64(i), 100000) == 1 {
		//	fmt.Print(".") // dot after 100k TODO remove
		//}
		var P Product
		Prices := []Price{{
			CurrencyId: "2",
		}, {}}
		Categories := []Category{{}, {}}
		Visibilities := []Visibility{{}}

		pid := uuid.New()
		P.Id = pid.String()
		P.Name = randomdata.SillyName()
		P.Description = randomdata.Paragraph()
		P.Stock = randomdata.Number(0, 2000)

		P.Price = Prices
		P.Categories = Categories
		P.Visibilities = Visibilities

		for _, storage := range s {
			failOnErr(storage.Save(&P))
		}
	}
}

func failOnErr(err error) {
	if err == nil {
		return
	}
	fmt.Println(err)
	os.Exit(1)
}
