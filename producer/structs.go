package producer

type Product struct {
	Id             string `fake:"skip"`
	Name           string `fake:"{person.first}"`
	Stock          int    `fake:"skip"`
	ManufacturerID string
	Description    string
	Tax            Tax
	Price          []Price
	ProductNumber  string
	Categories     []Category
	Visibilities   []Visibility
}

type Visibility struct {
	SalesChannelId string
	Visibility     int
}

type Category struct {
	Id string
}

type Price struct {
	CurrencyId string
	Gross      string
	Net        string
	linked     bool
}

type Tax struct {
	Name    string
	TaxRate float32
}
