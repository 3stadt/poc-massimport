package swapi

import (
	"gitlab.com/3stadt/poc-massimport/producer"
)

var batchSize = 1000

type prodChannel struct {
	channel  chan *producer.Product
	products []producer.Product
	arrayPos int
}

type swApi struct {
	ProdChannels []*prodChannel
}

func Open(threads, maxBatchSize int) *swApi {
	batchSize = maxBatchSize
	s := &swApi{ProdChannels: make([]*prodChannel, threads)}
	for i := 0; i < threads; i++ {
		s.ProdChannels[i] = &prodChannel{
			channel:  make(chan *producer.Product),
			products: make([]producer.Product, batchSize),
			arrayPos: 0,
		}
		s.ProdChannels[i].createReceiver()
	}
	return s
}

func (s *swApi) Close() {
	for _, pc := range s.ProdChannels {
		pc.Flush()
		pc.channel <- nil
	}
}

func (pc *prodChannel) Save(P *producer.Product) error {
	pc.channel <- P
	return nil
}

func (pc *prodChannel) Flush() {
	// TODO: Send current batch to SW6 API
	pc.products = make([]producer.Product, batchSize)
	pc.arrayPos = 0
}

func (pc *prodChannel) createReceiver() {
	go func() {
		for {
			product := <-pc.channel
			if product == nil {
				return
			}

			pc.products[pc.arrayPos] = *product
			pc.arrayPos++
			if pc.arrayPos == batchSize-1 {
				pc.Flush()
			}
		}
	}()
}
